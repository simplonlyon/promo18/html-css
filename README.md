# Projet HTML-CSS
Premier projet HTML-CSS pour la P18 simplon lyon

## Exercice Sélecteurs CSS [html](exo-selector.html)/[css](css/exo-selector.css)
1. Créer une balise section avec un id "first" qui contiendra un h1 avec First Section dedans
2. Dans la section créer un paragraphe avec "some text" dedans qui aura également une balise span avec une classe "highlight"  et comme contenu " and important text"
3. Dans le fichier CSS sélectionner le h1 à l'intérieur de la section first et mettre sa couleur en bleu et le mettre en gras
4. Ensuite, sélectionner la classe highlight et mettre sa couleur de fond en jaune et mettre son texte en italique
5. Dans le HTML, créer un nouveau paragraphe et lui mettre la  classe highlight et comme contenu "another paragraph", ainsi qu'un autre span avec la classe highlight dessus et " with more important text" comme contenu
6. Dans le css, faire une règle pour que les classe highlight à l'intérieur d'une classe highlight ait leur texte en bleu
7. Créer une nouvelle règle css qui mettra une bordure en pointillés autour des paragraphes qui sont dans un article (pour fêter ça,  faire un article avec un paragraphe dedans)
8. Mettre un h2 dans l'article puis faire une règle qui change la taille du texte des h2 pour 0.5em et tout en majuscule 


## Exercice Positionnement [html](position.html)/[css](css/position.css)
Faire une petite popup avec du texte et une croix de fermeture (non fonctionnelle pour le moment)

1. Créer une section avec un h1 et un paragraphe dedans avec ce que vous voulez (c'est juste le texte par dessus lequel sera mis le popup)
2. Rajouter un nouvel élément section avec un h2 Newsletter, un paragraphe Subscribe to newsletter et un input pour mettre l'email
3. Cibler la section dans le CSS et faire en sorte que ça ressemble à une popup, donc par exemple avec une bordure arrondie, avec un fond blanc et ce genre de chose
4. Modifier les règles de positions pour faire que le popup s'affiche au milieu de la page de manière fixe
5. Rajouter un bouton de fermeture dans la popup et le cibler pour faire qu'il aille se positionner en haut à droite de la popup 

(Pour voir que la popup suit bien, faire en sorte que la première section avec le h1 ait une height de 2000px ou 200vh par exemple) 

**Bonus:** Trouver une manière de faire qu'on ne puisse pas interagir avec les éléments derrières la popup (genre qu'on puisse pas sélectionner le texte du h1 avec la souris)